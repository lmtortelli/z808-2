/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z808;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

/**
 *
 * @author viniciusmoraes
 */
public class Linkador {
    
    String[] TabSim;
    BufferedReader buffRead;
    
    
    Linkador(String nome) throws FileNotFoundException{
        TabSim = new String[10];
        File macred = new File(nome);
        buffRead = new BufferedReader(new FileReader(macred));
    }
    
    public String[] Linkar() throws FileNotFoundException, IOException{
       
        
        return TabSim;
    }
    
     public static void main(String[] args) throws IOException {
        Linkador a = new Linkador("Teste.txt");
        a.Linkar();
    }
}
